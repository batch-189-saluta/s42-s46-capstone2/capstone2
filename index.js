const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const userRoutes = require('./routes/userRoutes.js')
const productRoutes = require('./routes/productRoutes.js')
const port = 4001

const app = express()

// express setup--------------------------------------------
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())


// mongoose setup----------------------------------------------
mongoose.connect('mongodb+srv://admin123:admin123@zuitt-bootcamp.rgocvci.mongodb.net/capstone2?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
})

let db = mongoose.connection
db.on('error', () => console.error.bind(console, 'error'))
db.on('open', () => console.log('Connected to MongoDB Atlas!'))


// routes----------------------------------------------------
app.use('/api/users', userRoutes)
app.use('/api/products', productRoutes)




// listen-----------------------------------------------------
app.listen(process.env.PORT || port, () => {console.log(`API is now online on port ${port}`)})