const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController.js')
const auth = require('../auth.js')

// check email------------------------------------------

router.post('/checkEmail', (req,res) => {
	userController.checkEmailExist(req.body).then(resultFromController=> res.send(resultFromController))
})

// User Registration------------------------------------

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


// User log in--------------------------------------------

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// get profile---------------------------------------------

router.get('/details', auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
})


// Set user as admin----------------------------------------------

router.put('/:userId/changeStatus', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {

		userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Failed: Authorized admin only!')
	}
})


// create order----------------------------------------------

router.post('/order', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	let data = {
		userId: userData.id,
		productId: req.body.productId,
		name: req.body.name,
		quantity: req.body.quantity,
		productCost: req.body.productCost
	}

	if(userData.isAdmin == true) {

		res.send('Failed: Admin cannot order')
	} else {

		userController.order(data).then(resultFromController => res.send(resultFromController))
	}
})


// retrieve all user's orders---------------------------------------

/*router.get('/:userId/myOrders', auth.verify, (req,res) => {

	userController.getOrders(req.params).then(resultFromController => res.send(resultFromController))
})*/

router.get('/myOrders', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getOrders({id: userData.id}).then(resultFromController => res.send(resultFromController))
})






// retrieve ALL orders------------------------------------------------

router.get('/allOrders', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {

		userController.getAllOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Failed: Authorized admin only!')
	}

})


// ------------------------------------------------
module.exports = router