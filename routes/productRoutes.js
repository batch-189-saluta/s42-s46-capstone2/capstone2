const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController.js')
const auth = require('../auth.js')


// register a product---------------------------------------------

router.post('/register', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {

		productController.registerProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {

		res.send("Failed: Authorized admin only") 
	}
	
})

// retrieve all products----------------------------------------------------

router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)


	productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});



// retrieve all active products-------------------------------------------

router.get('/', (req,res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
})


// retrieve single product------------------------------------------------------------

router.get('/:productId/', (req,res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// retrieve single product's orderList------------------------------------------------------------

router.get('/:productId/orderList', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {
		productController.getOrderList(req.params).then(resultFromController => res.send(resultFromController))
	} else {

		res.send('Failed: Authorized admin only!')
	}

	
})



// update a product----------------------------------------------------

router.put('/:productId/update', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Failed: Authorized admin only!')
	}
})


// archive a product---------------------------------------------

router.put('/:productId/archive', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin == true) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Failed: Authorized admin only!')
	}

})








// -------------------------------------------------------

module.exports = router