const User = require('../models/User.js')
const Product = require('../models/Product.js')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

// check email---------------------------------------------
module.exports.checkEmailExist =(reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

// register a user-------------------------------------------

module.exports.registerUser = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		
			let newUser = new User ({

				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((savedUser, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		
	})

	
}

// login user authentication----------------------------------------------

module.exports.loginUser =  (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			return "Create an account first"
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect == true) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// get profile-------------------------------------------------

module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {
		if (result ==null) {
			return false
		}

		result.password = ""
		return result
	})
}


// set user as admin----------------------------------------------

module.exports.updateUser = (reqParams, reqBody) => {

	let updatedStatus = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId, updatedStatus).then((user, error) => {
		if(error){
			return false
		} else {
			return "User is now an Admin! *clap, clap, clap*"
		}
	})
}


// order--------------------------------------------------

module.exports.order = async(data) => {

	let newProductCost = (data.productCost * data.quantity).toString()

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.myOrders.push({productId: data.productId, name:data.name, quantity: data.quantity, totalCost: newProductCost})
		

		return user.save().then((user,error) => {

			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		

		product.orders.push({userId: data.userId, quantity: data.quantity, totalCost: newProductCost})

		return product.save().then((course, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	

	if(isProductUpdated && isUserUpdated) {
		return true
	} else {
		return false
	}
}


// get user's orders---------------------------------------------

/*module.exports.getOrders = (reqParams) => {

	return User.findById(reqParams.userId).then(result => {

		return result.myOrders
	})
}*/

module.exports.getOrders = (data) => {

	return User.findById(data.id).then(result => {
		if (result ==null) {
			return false
		}

		return result.myOrders
	})
}


// retrieve ALL orders-----------------------------------------

module.exports.getAllOrders = () => {

	return Product.find({isActive: true}).then(result => {


		return result

	})
}