const Product = require('../models/Product.js')
const User = require('../models/User.js')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

// register a product-----------------------------------------------

module.exports.registerProduct = (reqBody) => {

	let newProduct = new Product ({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((savedProduct,error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

// Retrieve all products-------------------------------------------

module.exports.getAllProducts = async (data) => {

	if (data.isAdmin) {
		return Product.find({}).then(result => {

			return result
		})
	} else {

		return false
	}

}



// retrieve all active--------------------------------------------------------

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {

		let showOnly = []

		for(i=0; i<result.length; i++) {

			let newObject = {
				_id: result[i]._id,
				name: result[i].name,
				description: result[i].description,
				price: result[i].price
			}

			showOnly.push(newObject)

			
		}


		return showOnly
	})
}

// retrieve single product-------------------------------------------------

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		/*let showProduct = {
			name: result.name,
			description: result.description,
			price: result.price,
			isActive: result.isActive
		}*/


		return result
	})
}

// retrieve single product's order list-------------------------------------------------

module.exports.getOrderList = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		/*let showProduct = {
			name: result.name,
			description: result.description,
			price: result.price,
			isActive: result.isActive
		}*/


		return result.orders
	})
}



// update a product-------------------------------------------

module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}

// archive a product-------------------------------------------

module.exports.archiveProduct = (reqParams, reqBody) => {

	let archivedProduct = {
		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}





