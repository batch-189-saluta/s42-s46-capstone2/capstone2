const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},

	myOrders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			name: {
				type: String,
				// required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				default: 1
			},
			totalCost: {
				type: Number
				// required: [true, "Product cost is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

})

module.exports = mongoose.model("User", userSchema)