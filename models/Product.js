const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, 'Name of product is required']
	},
	description: {
		type: String,
		required: [true, 'Description is required']
	},
	price: {
		type: Number,
		required: [true, 'Price is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			quantity: {
				type: Number,
				default: 1
			},
			totalCost: {
				type: Number
			},
			purchasedOn: {
				type: String,
				default: new Date()
			}
		}
	]	
})


module.exports = mongoose.model("Product", productSchema)